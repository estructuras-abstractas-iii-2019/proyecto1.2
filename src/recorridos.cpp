#include "recorridos.h"

using namespace lemon;

Recorrido::~Recorrido(){}

Recorrido::Recorrido(ListGraph& grafo, ListGraph::EdgeMap<int>& mapa){
    this->cant_Nodos = countNodes(grafo);
    this->matAdyacencia = new int*[cant_Nodos];
    
    for(int i=0; i < cant_Nodos; ++i){
        this->matAdyacencia[i] = new int[cant_Nodos];
        for (int j = 0; j < this->cant_Nodos; ++j){
            this->matAdyacencia[i][j]=0;
        }
    }

    for(ListGraph::EdgeIt it(grafo); it != INVALID; ++it){
        this->matAdyacencia[grafo.id(grafo.u(it))][grafo.id(grafo.v(it))]=mapa[it];
        this->matAdyacencia[grafo.id(grafo.v(it))][grafo.id(grafo.u(it))]=mapa[it]; 
    }
}

void Recorrido::anchitud(int inicio){ 
    std::cout << std::endl << "El recorrido por anchitud del grafo es: " << std::endl;
    std::list<int> cola;
    cola.push_back(inicio);

    bool visitado[this->cant_Nodos];
    visitado[inicio] = true;

    do{
        inicio = cola.front();
        std::cout << inicio << " - ";
        cola.pop_front();

        for (int i = 0; i < this->cant_Nodos; ++i){
            if(this->matAdyacencia[inicio][i]!=0){
                if ( !visitado[i] ){
                    visitado[i] = true;
                    cola.push_back(i);
                }
            }            
        }
    } while(!cola.empty());

    std::cout<<std::endl;
}

void Recorrido::profundidad(int inicio){
    std::cout << std::endl << "El recorrido por profundidad del grafo es: " << std::endl; 
    bool visitado[this->cant_Nodos];
    DFS(inicio, visitado); 
    std::cout<<std::endl<<std::endl;;
}

void Recorrido::DFS(int inicio, bool* visitado){ 
    visitado[inicio] = true; 
    std::cout << inicio << " - "; 

    for (int i = 0; i < this->cant_Nodos; ++i){
        if(this->matAdyacencia[inicio][i]!=0){
            if ( !visitado[i] ){ DFS(i, visitado); }
        }            
    }
}

void Recorrido::prim(){
    std::cout << std::endl << "El árbol de expansión mínima generado al aplicar el Algoritmo de Prim al grafo posee la siguiente estructura:" << std::endl<< std::endl;
    int peso[this->cant_Nodos]={0};//Peso 0 para el primer elemento  
    int arbolPrim[this->cant_Nodos]={-1};
    bool agregadoAlArbol[this->cant_Nodos];  

    for (int i = 1; i < this->cant_Nodos; i++){  
        peso[i] = ENTERO_MAXIMO;
    }
  
    for (int count = 0; count < this->cant_Nodos - 1; count++){  
        int porAgregar = pesoMin(peso, agregadoAlArbol);  
        agregadoAlArbol[porAgregar] = true;  
  
        for (int k = 0; k < this->cant_Nodos; k++){ 
            if (this->matAdyacencia[porAgregar][k] && agregadoAlArbol[k] == false && this->matAdyacencia[porAgregar][k] < peso[k])  
                arbolPrim[k] = porAgregar, peso[k] = this->matAdyacencia[porAgregar][k];  
        }  
    }  
  
    mostrarArbol(arbolPrim); 
    std::cout<<std::endl; 
}  
  
int Recorrido::pesoMin(int* peso, bool* agregadoAlArbol)  {  
    int minimo = ENTERO_MAXIMO;
    int indiceMinimo;  
    for (int i = 0; i < this->cant_Nodos; i++)  
        if (agregadoAlArbol[i] == false && peso[i] < minimo){
            minimo = peso[i];
            indiceMinimo = i;  
        }  
    return indiceMinimo;  
}  
  
void Recorrido::mostrarArbol(int* arbolPrim){  
    std::cout<<"Arista \tCosto"<<std::endl;  
    for (int i = 1; i < this->cant_Nodos; i++)  
        std::cout<<arbolPrim[i]<<" - "<<i<<" \t"<<this->matAdyacencia[i][arbolPrim[i]]<<std::endl;  
}  