#include "recorridos.h"

using namespace lemon;

int main(int argc, char const *argv[])
{
    std::cout<<std::endl<<"El presente es un programa de pruebas para los algoritmos de recorrido de grafos que se crearon.";
    std::cout<<" Presione < ENTER > para ver los recorridos de los dos grafos incluidos en el enunciado del proyecto"<<std::endl;
    std::getchar();

	ListGraph primero;

    ListGraph::Node a = primero.addNode();
    ListGraph::Node b = primero.addNode();
    ListGraph::Node c = primero.addNode();
    ListGraph::Node d = primero.addNode();
    ListGraph::Node e = primero.addNode();
    ListGraph::Node f = primero.addNode();
    ListGraph::Node g = primero.addNode();
    ListGraph::Node h = primero.addNode();
    ListGraph::Node i = primero.addNode();

    ListGraph::Edge ab = primero.addEdge(a, b);
    ListGraph::Edge ah = primero.addEdge(a, h);
    ListGraph::Edge bc = primero.addEdge(b, c);
    ListGraph::Edge bh = primero.addEdge(b, h);
    ListGraph::Edge cd = primero.addEdge(c, d);
    ListGraph::Edge cf = primero.addEdge(c, f);
    ListGraph::Edge ci = primero.addEdge(c, i);
    ListGraph::Edge de = primero.addEdge(d, e);
    ListGraph::Edge df = primero.addEdge(d, f);
    ListGraph::Edge ef = primero.addEdge(e, f);
    ListGraph::Edge fg = primero.addEdge(f, g);
    ListGraph::Edge gh = primero.addEdge(g, h);
    ListGraph::Edge gi = primero.addEdge(g, i);
    ListGraph::Edge hi = primero.addEdge(h, i);

    ListGraph::EdgeMap<int> costosPrimero(primero);
    costosPrimero[ab] = 4;
    costosPrimero[ah] = 8;
    costosPrimero[bc] = 8;
    costosPrimero[bh] = 11;
    costosPrimero[cd] = 7;
    costosPrimero[cf] = 4;
    costosPrimero[ci] = 2;
    costosPrimero[de] = 9;
    costosPrimero[df] = 14;
    costosPrimero[ef] = 10;
    costosPrimero[fg] = 2;
    costosPrimero[gh] = 1;
    costosPrimero[gi] = 6;
    costosPrimero[hi] = 7;

    std::cout<<"\t>>>>>>>>>>>>>>>>>>>>>>>>>>PRIMER GRAFO<<<<<<<<<<<<<<<<<<<<<<<<<<"<<std::endl<<std::endl;

    std::cout<<"** INICIANDO DESDE EL NODO 0 **"<<std::endl;
    Recorrido primerGrafo(primero, costosPrimero);
    primerGrafo.anchitud(0);
    primerGrafo.profundidad(0);
	primerGrafo.prim();

    //Segundo Grafo
    ListGraph segundo;

    ListGraph::Node z = segundo.addNode();//a
    ListGraph::Node y = segundo.addNode();//b
    ListGraph::Node x = segundo.addNode();//c
    ListGraph::Node w = segundo.addNode();//d
    ListGraph::Node v = segundo.addNode();//e
    ListGraph::Node u = segundo.addNode();//f
    ListGraph::Node t = segundo.addNode();//g
    ListGraph::Node s = segundo.addNode();//h
    ListGraph::Node r = segundo.addNode();//i
    ListGraph::Node q = segundo.addNode();//j

	ListGraph::Edge zu = segundo.addEdge(z, u);
	ListGraph::Edge zs = segundo.addEdge(z, s);
	ListGraph::Edge zr = segundo.addEdge(z, r);
	ListGraph::Edge yu = segundo.addEdge(y, u);
	ListGraph::Edge yt = segundo.addEdge(y, t);
	ListGraph::Edge ys = segundo.addEdge(y, s);
	ListGraph::Edge yq = segundo.addEdge(y, q);
	ListGraph::Edge xu = segundo.addEdge(x, u);
	ListGraph::Edge xv = segundo.addEdge(x, v);
	ListGraph::Edge wt = segundo.addEdge(w, t);
	ListGraph::Edge wq = segundo.addEdge(w, q);
	ListGraph::Edge vu = segundo.addEdge(v, u);
	ListGraph::Edge vq = segundo.addEdge(v, q);
	ListGraph::Edge ts = segundo.addEdge(t, s);

	ListGraph::EdgeMap<int> costosSegundo(segundo);
	costosSegundo[zu] = 343;
	costosSegundo[zs] = 1435;
	costosSegundo[zr] = 464;
	costosSegundo[yu] = 879;
	costosSegundo[yt] = 954;
	costosSegundo[ys] = 811;
	costosSegundo[yq] = 524;
	costosSegundo[xu] = 1054;
	costosSegundo[xv] = 1364;
	costosSegundo[wt] = 433;
	costosSegundo[wq] = 1053;
	costosSegundo[vu] = 1106;
	costosSegundo[vq] = 766;
	costosSegundo[ts] = 837;

    std::cout<<"\t>>>>>>>>>>>>>>>>>>>>>>>>>>SEGUNDO GRAFO<<<<<<<<<<<<<<<<<<<<<<<<<<"<<std::endl<<std::endl;

    std::cout<<"** INICIANDO DESDE EL NODO 0 **"<<std::endl;
    Recorrido segundoGrafo(segundo, costosSegundo);
    segundoGrafo.anchitud(0);
    segundoGrafo.profundidad(0);
    segundoGrafo.prim();

	return 0;
}