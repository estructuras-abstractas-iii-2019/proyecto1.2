TARGET = graph

CCX = g++
CFLAGS = -Wall -g -lemon

LINKER = g++
LFLAGS = -I.

SRCDIR = src
OBJDIR = build
BINDIR = bin
INCDIR = include

SOURCES := $(wildcard $(SRCDIR)/*.cpp)
INCLUDES := $(wildcard $(INCDIR)/*.h)
OBJECTS := $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
rm = rm -f


all: $(BINDIR)/$(TARGET)


$(BINDIR)/$(TARGET): $(OBJECTS)
	@$(LINKER) $(OBJECTS) $(LFLAGS) -o $@
	@clear
	@echo "Ejecute el comando ./bin/graph para empezar"


$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	@$(CCX) $(CFLAGS) -I$(INCDIR) -I$(SRCDIR) -c $< -o $@


clean:
	@$(rm) $(OBJECTS)
	@$(rm) $(OBJDIR)/*.o
	@$(rm) $(BINDIR)/$(TARGET)


.PHONY: all clean