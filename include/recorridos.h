#pragma once

/**
 * @author Alexander Calderón Torres
 * @author Roberto Acevedo Mora
 * @date 29/02/2020
 * 
 * @file recorridos.h
 * @brief Definición de la clase Recorrido
 */

#include <iostream>
#include <string>
#include <lemon/list_graph.h>
#include <list>
#include <string>

#define ENTERO_MAXIMO 2147483647

using namespace lemon;

/**
 * @class Recorrido
 * @brief En esta clase se incluyen los metodos necesarios para 
 * @brief llevar a cabo los recorridos por anchitud, profundidad y el algoritmo de Prim.
 */

class Recorrido{
  public:
  
   /** 
     * @brief Constructor de la clase recorrido.
     */

    Recorrido(ListGraph& grafo, ListGraph::EdgeMap<int>& mapa);
    
    /** 
     * @brief Destructor 
     */
     
     
    ~Recorrido();
    
    /** 
     * @brief Método que recibe el nodo actual y lo marca como visitado. Recorre los vertices adyacentes.
     */

    void DFS(int inicio, bool* visitado); 
    
    /** 
     * @brief Método que recibe como parametro cualquiera de los nodos disponibles en el grafo
     * @brief e imprime el recorrido por anchitud.
     */

    void anchitud(int inicio);
    
    /** 
     * @brief Método que recibe un nodo y utiliza el metodo DFS para imprimer el recorrdio a profundidad.
     */
     
    void profundidad(int inicio);
    
    /** 
     * @brief Metodo que se utiliza para construir e imprimir el arbol recubridor minimo 
     * utilizando la matriz de adyacencia.
     */
    
    void prim();
    
    /** 
     * @brief Método que se ejecuta para encontrar el vértice con
     * valor de peso minimo del conjunto de vértices.
     */
    
    int pesoMin(int* key, bool* agregado);
    
    /** 
     * @brief Método que se utiliza para imprimir el arbol recubridor minimo.
     */
     
    void mostrarArbol(int* arbolPrim);

  private:
    int cant_Nodos;
    int** matAdyacencia;
};